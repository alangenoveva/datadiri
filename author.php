<?php
require_once('config/koneksi.php');
?>

<!DOCTYPE html>
<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="css/index.css">
</head>


<body>
    <!-- navbar -->
    <nav id="navbar" class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">DataDiri.ss</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Beranda</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="history.php">History</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="kontak.php">Kontak</a>
                    </li>
                </ul>                 
            </div>
        </div>
    </nav>

    <!-- konten -->
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
                <h3>Author</h3>

                <?php
                $order = $_GET['order'] ?? 'asc';
                if(!in_array($order, ['asc', 'desc'])){
                    $order = 'asc';
                }

                $sql = "SELECT * FROM `author` order by id $order";
                $authors = $connection->query($sql);
                
                if($authors->num_rows > 0){
                    foreach ($authors as $key => $author) {
                        echo '<p><a href="article.php?author='.$author['id'].'">';
                        echo $author['id'] . ". ";
                        echo $author['email'] . " | ";
                        echo $author['name'];  
                        echo "</a></p>";
                    }
                }
                ?>
            </div>
        </div>
    </div>

    <!-- footer -->
</body>

</html>
