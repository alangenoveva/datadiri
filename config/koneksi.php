<?php

// $connection = mysqli_connect('localhost' , 'root' , '' , 'drupalsatu');
// if($connection){
//     echo "berhasil";
// }else{
//     echo "koneksi gagal";
// }

class Database{

    private $hostname = 'localhost';
    private $username = 'root';
    private $password = '';
    private $db = 'latihansalt';

    public function connect(){
        $conn = mysqli_connect($this->hostname, $this->username, $this->password, $this->db);
        if (!$conn) {
            echo "koneksi gagal";
        }

        return $conn;
    }

}

$database = new Database();
$connection = $database->connect();
